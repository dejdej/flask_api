#!/usr/bin

sudo yum install -y epel-release python python-pip

sudo pip install flask flask-mysql

# If you come across a certification validation error while running the above command, please use the below command.

# sudo pip install --trusted-host files.pythonhosted.org --trusted-host pypi.org --trusted-host pypi.python.org flask flask-mysql

