* Master Branch:
[![pipeline status](https://gitlab.com/dejdej/flask_api/badges/master/pipeline.svg)](https://gitlab.com/dejdej/flask_api/commits/master)


# flask_app

- if flask CLI is used to serve wsgi `flask run --host=0.0.0.0`, add FLASK_APP as env variable  
```bash
$ export FLASK_APP=hello
$ flask run
```

# flask_API

**Endpoints**:

* /   
Returns the system properties

* /data   
Return all the data from data_client

* /data/user?id=integer_value   
Return data for an user, based on his id sent in URL query


* /data/name    
POST request: `curl -i -X POST -H "Content-Type: application/json" -d '{"name":"your_parameter"}' 127.0.0.1:5000/data/name`